<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 * @todo        catch error (and show alert) if postfix email already exists
 * @todo        extend Tinebase_Controller_Record_Abstract
 */

/**
 * User Controller for Admin application
 *
 * @package     Admin
 * @subpackage  Controller
 */
class Admin_Controller_User extends Tinebase_Controller_Abstract
{
    /**
     * @var Tinebase_User_Abstract
     */
    protected $_userBackend = NULL;
    
    /**
     * @var Tinebase_SambaSAM_Ldap
     */
    protected $_samBackend = NULL;

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() 
    {
        $this->_applicationName = 'Admin';
        
        $this->_userBackend = Tinebase_User::getInstance();
    }

    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone() 
    {
    }

    /**
     * holds the instance of the singleton
     *
     * @var Admin_Controller_User
     */
    private static $_instance = NULL;

    /**
     * the singleton pattern
     *
     * @return Admin_Controller_User
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Admin_Controller_User;
        }
        
        return self::$_instance;
    }

    /**
     * get list of full accounts -> renamed to search full users
     *
     * @param array $_filter array to search accounts for
     * @param string $_sort
     * @param string $_dir
     * @param int $_start
     * @param int $_limit
     * @return Tinebase_Record_RecordSet with record class Tinebase_Model_FullUser
     */
    public function searchFullUsers($_filter, $_sort = NULL, $_dir = 'ASC', $_start = NULL, $_limit = NULL)
    {
        $this->checkRight('VIEW_ACCOUNTS');
        
        $filter = new Tinebase_Model_UserFilter($_filter, 'OR');

        $result = $this->_userBackend->getUsersWithFilterGroup($filter, $_sort, $_dir, $_start, $_limit, 'Tinebase_Model_FullUser');
        
        return $result;
    }
    
    /**
     * count users
     *
     * @param string $_filter string to search user accounts for
     * @return int total user count
     */
    public function searchCount($_filter)
    {
        $this->checkRight('VIEW_ACCOUNTS');
        
        return $this->_userBackend->getUsersCount($_filter, 'Tinebase_Model_FullUser');
    }
    
    /**
     * count users using filter group
     *
     * @param array $_filter string to search user accounts for
     * @return int total user count
     */
    public function searchCountWithFilterGroup($_filter)
    {
        $this->checkRight('VIEW_ACCOUNTS');

        $filter = new Tinebase_Model_UserFilter($_filter, 'OR');

        return $this->_userBackend->getUsersCountWithFilterGroup($filter, 'Tinebase_Model_FullUser');
    }

    /**
     * get account
     *
     * @param   string  $_accountId  account id to get
     * @return  Tinebase_Model_FullUser
     */
    public function get($_userId)
    {
        $this->checkRight('VIEW_ACCOUNTS');

        $sqlUser = $this->_userBackend->getUserById($_userId, 'Tinebase_Model_FullUser');
        if($sqlUser->accountNotFindInSyncBackend){
            return $sqlUser;
        }
        return $sqlUser;
    }
    
    /**
     * set account status
     *
     * @param   string $_accountId  account id
     * @param   string $_status     status to set
     * @return  array with success flag
     */
    public function setAccountStatus($_accountId, $_status)
    {
        $this->checkRight('MANAGE_ACCOUNTS');

        if(Tinebase_Acl_Roles::getInstance()->hasRight($this->_applicationName, $_accountId, 'ADMIN')){
             $this->checkRight('ADMIN');
        }
        
        $result = $this->_userBackend->setStatus($_accountId, $_status);
        
        return $result;
    }
    
    /**
     * set the password for a given account
     *
     * @param  Tinebase_Model_FullUser  $_account the account
     * @param  string                   $_password the new password
     * @param  string                   $_passwordRepeat the new password again
     * @param  bool                     $_mustChange
     * @return void
     * 
     * @todo add must change pwd info to normal tine user accounts
     */
    public function setAccountPassword(Tinebase_Model_FullUser $_account, $_password, $_passwordRepeat, $_mustChange = false)
    {
        $this->checkRight('MANAGE_ACCOUNTS');

        if(Tinebase_Acl_Roles::getInstance()->hasRight($this->_applicationName, $_account->accountId, 'ADMIN')){
             $this->checkRight('ADMIN');
        }
        
        if ($_password != $_passwordRepeat) {
            throw new Admin_Exception("Passwords don't match.");
        }
        
        $this->_userBackend->setPassword($_account, $_password, true, $_mustChange);
        
        Tinebase_Core::getLogger()->info(
            __METHOD__ . '::' . __LINE__ . 
            ' Set new password for user ' . $_account->accountLoginName . '. Must change:' . $_mustChange
        );
    }
    
    /**
     * update user
     *
     * @param  Tinebase_Model_FullUser    $_user            the user
     * @param  string                     $_password        the new password
     * @param  string                     $_passwordRepeat  the new password again
     * @param  Array                      $_contact         the user data
     * 
     * @return Tinebase_Model_FullUser
     */
    public function update(Tinebase_Model_FullUser $_user, $_password, $_passwordRepeat, array $_contact)
    {
        $this->checkRight('MANAGE_ACCOUNTS');

        if(Tinebase_Acl_Roles::getInstance()->hasRight($this->_applicationName, $_user->accountId, 'ADMIN')){
             $this->checkRight('ADMIN');
        }
        
        $oldUser = $this->_userBackend->getUserByProperty('accountId', $_user, 'Tinebase_Model_FullUser');
        
        if ($oldUser->accountLoginName !== $_user->accountLoginName) {
            $this->_checkLoginNameExistance($_user);
        }

        if ($oldUser->accountEmailAddress !== $_user->accountEmailAddress) {
            $this->_checkEmailExistence($_user);
        }
        if($oldUser->accountExpires !== $_user->accountExpires){
            $this->_checkDisableDate($_user);
        }

        if($_user->smtpUser){
            $aliasesErrors = $_user->smtpUser->validateExistentAliases();
            foreach ( $aliasesErrors as $error) {
                if(!in_array($error,$oldUser->smtpUser->emailAliases)){
                    $finalErrors[] = $error;
                }
            }
            if (count($finalErrors) > 0){
                $translate = Tinebase_Translation::getTranslation('Admin');
                throw new Tinebase_Exception_SystemGeneric(sprintf($translate->_('The email alias %1$s is already in system. Please choose another alias'), implode(', ',$finalErrors)));
            }
        }

        $this->_checkPrimaryGroupExistance($_user);
        
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Update user ' . $_user->accountLoginName);
        
        try {
            $transactionId = Tinebase_TransactionManager::getInstance()->startTransaction(Tinebase_Core::getDb());
            
            if (Tinebase_Application::getInstance()->isInstalled('Addressbook') === true) {
                $_user->contact_id = $oldUser->contact_id;
                $contact = $this->createOrUpdateContact($_user, $_contact);
                $_user->contact_id = $contact->getId();
            }
            
            $user = $this->_userBackend->updateUser($_user);
    
            // make sure primary groups is in the list of groupmemberships
            $groups = array_unique(array_merge(array($user->accountPrimaryGroup), (array) $_user->groups));
            Admin_Controller_Group::getInstance()->setGroupMemberships($user, $groups);
            
            Tinebase_TransactionManager::getInstance()->commitTransaction($transactionId);

            //History data: will add a note about update account
            try{
                $historyUser = $this->_jsonDataValid($_user);
                $historyUser['contact'] = $this->_jsonDataValid($_contact);

                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Updating account event:  " . print_r($historyUser, true));

                Tinebase_Timemachine_ModificationLog::getInstance()->setRecordMetaData($user, 'update', $oldUser);
                Tinebase_Timemachine_ModificationLog::getInstance()->writeModLog($user, $oldUser, get_class($_user), 'Sql', $user->getId());

                $translate = Tinebase_Translation::getTranslation('Admin');
                $noteMessage = $translate->_('User account changed') . ' | ' . $translate->_('All input data') . ': ';
                $noteMessage .= json_encode($historyUser);
                Tinebase_Notes::getInstance()->addSystemNote($_user, Tinebase_Core::getUser(), Tinebase_Model_Note::SYSTEM_NOTE_NAME_CHANGED, $noteMessage, 'Sql', 'Tinebase_Model_FullUser');
            }
            catch (Tinebase_Exception_UnexpectedValue $tmv){
                Tinebase_Core::getLogger()->error(__METHOD__ . '::' . __LINE__ . " User account history entry fails to be changed " . print_r($tmv, true));
                throw new Tinebase_Exception_InvalidArgument("History log update entry for user account fails");
            }
            
        } catch (Exception $e) {
            Tinebase_TransactionManager::getInstance()->rollBack();
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' ' . $e);
            
            if ($e instanceof Zend_Db_Statement_Exception && preg_match('/Lock wait timeout exceeded/', $e->getMessage())) {
                throw new Tinebase_Exception_Backend_Database_LockTimeout($e->getMessage());
            }
            
            throw $e;
        }
        
        // fire needed events
        $event = new Admin_Event_UpdateAccount;
        $event->account = $user;
        $event->oldAccount = $oldUser;
        Tinebase_Event::fireEvent($event);
        
        if (!empty($_password) && !empty($_passwordRepeat)) {
            $this->setAccountPassword($_user, $_password, $_passwordRepeat, true);
        }

        return $user;
    }
    
    /**
     * create user
     *
     * @param  Tinebase_Model_FullUser  $_account           the account
     * @param  string                   $_password           the new password
     * @param  string                   $_passwordRepeat  the new password again
     * @param  array                    $_contact
     * @return Tinebase_Model_FullUser
     */
    public function create(Tinebase_Model_FullUser $_user, $_password, $_passwordRepeat, array $_contact)
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        
        // avoid forging accountId, gets created in backend
        unset($_user->accountId);
        
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Create new user ' . $_user->accountLoginName);
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . print_r($_user->toArray(), TRUE));
        
        $this->_checkLoginNameExistance($_user);
        $this->_checkEmailExistence($_user);
        $this->_checkPrimaryGroupExistance($_user);
        $this->_checkMailboxExistence($_user);
        $_user = $this->_checkDisableDate($_user);
        if($_user->smtpUser){
            $aliasesErrors = $_user->smtpUser->validateExistentAliases();
            if (count($aliasesErrors) > 0){
                $translate = Tinebase_Translation::getTranslation('Admin');
                throw new Tinebase_Exception_SystemGeneric(sprintf($translate->_('The email alias %1$s is already in system. Please choose another alias'), implode(' , ',$aliasesErrors)));
            }
        }
        
        if ($_password != $_passwordRepeat) {
            throw new Admin_Exception("Passwords don't match.");
        } else if (empty($_password)) {
            $_password = '';
            $_passwordRepeat = '';
        }
        Tinebase_User::getInstance()->checkPasswordPolicy($_password, $_user);

        if($this->_userBackend instanceof Tinebase_User_Ldap){
            try{
                $userFromSyncBackend = $this->_userBackend->addUserToSyncBackend($_user);
                $_user->accountId = $userFromSyncBackend->accountId;
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . 'Creating user in LDAP, AccountID = '.$userFromSyncBackend->accountId);
                $groups = array_unique(array_merge(array($_user->accountPrimaryGroup), (array) $_user->groups));
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . 'Adding to groups '. implode(',',$groups));
                foreach($groups as $group){
                    if($group){
                        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . 'Adding to group '. $group);
                        Tinebase_Group::getInstance()->addGroupMemberInSyncBackend($group, $userFromSyncBackend->accountId);
                        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . 'Added to group '. $group);
                    }
                }
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . 'Adding user '.$userFromSyncBackend->accountId.'  in groups');
                $transactionId = Tinebase_TransactionManager::getInstance()->startTransaction(Tinebase_Core::getDb());
                Tinebase_User::syncUser($_user,array('syncContactData' => TRUE, 'ignoreSyncWhenNotFound' => TRUE));
                Tinebase_TransactionManager::getInstance()->commitTransaction($transactionId);
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . 'Syncing user '.$userFromSyncBackend->accountId.' to database');
            }catch (Exception $e) {
                if(isset($userFromSyncBackend) && isset($groups)){
                    foreach($groups as $group){
                        if($group){
                            Tinebase_Group::getInstance()->removeGroupMemberInSyncBackend($group, $userFromSyncBackend->accountId);
                        }
                    }
                    $this->_userBackend->deleteUserInSyncBackend($userFromSyncBackend->accountId);
                }
                if(isset($transactionId)){
                    Tinebase_TransactionManager::getInstance()->rollBack();
                }
                if (Tinebase_Core::isLogLevel(Zend_Log::ERR)) Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' ' . $e->getMessage());
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . $e->getTraceAsString());
                $translate = Tinebase_Translation::getTranslation('Admin');
                throw $e;
            }
            $user = Tinebase_User::getInstance()->getUserById($userFromSyncBackend->accountId,'Tinebase_Model_FullUser');
        }else{
            try {
                if (Tinebase_Application::getInstance()->isInstalled('Addressbook') === true) {
                    $contact = $this->createOrUpdateContact($_user, $_contact);
                    $_user->contact_id = $contact->getId();
                }
                $transactionId = Tinebase_TransactionManager::getInstance()->startTransaction(Tinebase_Core::getDb());
                $user = $this->_userBackend->addUser($_user,$_contact);
                // make sure primary groups is in the list of groupmemberships
                $groups = array_unique(array_merge(array($user->accountPrimaryGroup), (array) $_user->groups));
                Admin_Controller_Group::getInstance()->setGroupMemberships($user, $groups);

                Tinebase_TransactionManager::getInstance()->commitTransaction($transactionId);

            } catch (Exception $e) {
                Tinebase_TransactionManager::getInstance()->rollBack();
                Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' ' . $e->getMessage());
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . $e->getTraceAsString());
                throw $e;
            }
        }
        
         try{
            //History: will create a new entry at 'Notes' table, on user creation
            $noteUser = $this->_jsonDataValid($_user);
            $noteUser['contact'] = $this->_jsonDataValid($_contact);

            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Updating account event:  " . print_r($noteUser, true));

            Tinebase_Timemachine_ModificationLog::getInstance()->setRecordMetaData($_user, 'create');
            Tinebase_Timemachine_ModificationLog::getInstance()->writeModLog($_user, $_user, get_class($_user), 'Sql', $_user->getId());

            $translate = Tinebase_Translation::getTranslation('Admin');
            $noteMessage = $translate->_('User account created') . ' | ' . $translate->_('Original input data') . ': ';
            $noteMessage .= json_encode($noteUser);
            Tinebase_Notes::getInstance()->addSystemNote($_user, Tinebase_Core::getUser(), Tinebase_Model_Note::SYSTEM_NOTE_NAME_CREATED, $noteMessage, 'Sql', 'Tinebase_Model_FullUser');
        }
        catch (Tinebase_Exception_UnexpectedValue $tmv){
                Tinebase_Core::getLogger()->error(__METHOD__ . '::' . __LINE__ . " User account history entry fails to be created." . print_r($tmv, true));
                throw new Tinebase_Exception_InvalidArgument("History log create entry for user account fails.");
            }

        $event = new Admin_Event_AddAccount(array(
            'account' => $user
        ));
        Tinebase_Event::fireEvent($event);
        
        $this->setAccountPassword($user, $_password, $_passwordRepeat,true);

        return $user;
    }
    
    /**
     * look for user with the same login name
     * 
     * @param Tinebase_Model_FullUser $user
     * @return boolean
     * @throws Tinebase_Exception_SystemGeneric
     */
    protected function _checkLoginNameExistance(Tinebase_Model_FullUser $user)
    {
        try {
            $existing = Tinebase_User::getInstance()->getUserByLoginName($user->accountLoginName);
            if ($user->getId() === NULL || $existing->getId() !== $user->getId()) {
                throw new Tinebase_Exception_SystemGeneric('Login name already exists. Please choose another one.');
            }
        } catch (Tinebase_Exception_NotFound $tenf) {
        }
        
        return TRUE;
    }

    /**
     * Check the existence of a email for save and update
     *
     * @param Tinebase_Model_FullUser $_user
     * @throws Tinebase_Exception_SystemGeneric
     */
    protected function _checkEmailExistence(Tinebase_Model_FullUser $_user)
    {
        $arrReturn = $this->checkEmail(array($_user->accountEmailAddress));
        if ($arrReturn['status']==='failure'){
            $translate = Tinebase_Translation::getTranslation('Admin');
            throw new Tinebase_Exception_SystemGeneric($translate->_('The email informed is already in use'));
        }
    }

    /**
    * Check the existence of the user mailbox.
    *
    * @param Tinebase_Model_FullUser $_user
    * @throws Tinebase_Exception_SystemGeneric
    */
    protected function _checkMailboxExistence(Tinebase_Model_FullUser $_user)
    {
        if($this->_userBackend->checkMailbox($_user)){
            throw new Tinebase_Exception_SystemGeneric(Tinebase_Translation::getTranslation('Admin')->_('Mailbox in use, plese select other login name'));
        }
    }

     /**
     * Check the expiration date of the user.
     *
     * @param Tinebase_Model_FullUser $_user
     * @return Tinebase_Model_FullUser
     * @throws Tinebase_Exception_SystemGeneric
     */
    protected function _checkDisableDate(Tinebase_Model_FullUser $_user)
    {
       $expiradate = $_user->accountExpires;
       $now = Tinebase_DateTime::now();
       if($expiradate && $expiradate->compare($now,'ymd') == 0){
           $_user->accountExpires = $now;
           return $_user;
       }elseif($expiradate && $expiradate->compare($now,'ymd') == -1){
            $translate = Tinebase_Translation::getTranslation('Admin');
            throw new Tinebase_Exception_SystemGeneric($translate->_("you can't set a past date do disable a user"));
        }else{
            return $_user;
        }
    }

    /**
     * look for primary group, if it does not exist, fallback to default user group
     * 
     * @param Tinebase_Model_FullUser $user
     * @throws Tinebase_Exception_SystemGeneric
     */
    protected function _checkPrimaryGroupExistance(Tinebase_Model_FullUser $user)
    {
        try {
            $group = Tinebase_Group::getInstance()->getGroupById($user->accountPrimaryGroup);
        } catch (Tinebase_Exception_Record_NotDefined $ternd) {
            $defaultUserGroup = Tinebase_Group::getInstance()->getDefaultGroup();
            if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
                . ' Group with id ' . $user->accountPrimaryGroup . ' not found. Use default group (' . $defaultUserGroup->name
                . ') as primary group for ' . $user->accountLoginName);
            
            $user->accountPrimaryGroup = $defaultUserGroup->getId();
        }
    }
    
    /**
     * delete accounts
     *
     * @param   mixed $_accountIds  array of account ids
     * @return  array with success flag
     * @throws  Tinebase_Exception_Record_NotAllowed
     */
    public function delete($_accountIds)
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        
        $groupsController = Admin_Controller_Group::getInstance();
        
        foreach ((array)$_accountIds as $accountId) {

            if(Tinebase_Acl_Roles::getInstance()->hasRight($this->_applicationName,$accountId, 'ADMIN')){
                $this->checkRight('ADMIN');
            }

            if ($accountId === Tinebase_Core::getUser()->getId()) {
                $message = 'You are not allowed to delete yourself!';
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' ' . $message);
                throw new Tinebase_Exception_AccessDenied($message);
            }

            Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " about to remove user with id: {$accountId}");

            $oldUser = $this->get($accountId);
            //Delete from groups
            $memberships = $groupsController->getGroupMemberships($accountId);
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " removing user from groups: " . print_r($memberships, true));

            foreach ((array)$memberships as $groupId) {
                $groupsController->removeGroupMember($groupId, $accountId);
            }
            if (Tinebase_Application::getInstance()->isInstalled('Addressbook') === true && !empty($oldUser->contact_id)) {
                $this->_deleteContact($oldUser->contact_id);
            }

            if(isset($oldUser->accountLastLogin) && !$oldUser->accountNotFindInSyncBackend){
                $option = Tinebase_Config::getInstance()->get(Tinebase_Config::USERBACKEND, new Tinebase_Config_Struct())->toArray();
                $this->_userBackend = new Admin_Backend_LdapUser($option);
                $imap = new Admin_Backend_MailUser();
                $imap->logicDeleteUser($oldUser);
            }

            $this->_userBackend->deleteUser($accountId);
             //History data: will add a note about drop account
            try{
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Deleting account event:  " . print_r($oldUser, true));

                Tinebase_Timemachine_ModificationLog::getInstance()->setRecordMetaData($oldUser, 'delete', $oldUser);
                Tinebase_Timemachine_ModificationLog::getInstance()->writeModLog($oldUser, $oldUser, get_class($oldUser), 'Sql', $oldUser->getId());

                $translate = Tinebase_Translation::getTranslation('Admin');
                $noteMessage = $translate->_('User account  deleted');
                Tinebase_Notes::getInstance()->addSystemNote($oldUser, Tinebase_Core::getUser(), Tinebase_Model_Note::SYSTEM_NOTE_NAME_CHANGED, $noteMessage, 'Sql', 'Tinebase_Model_FullUser');
            }
            catch (Tinebase_Exception_UnexpectedValue $tmv){
                Tinebase_Core::getLogger()->error(__METHOD__ . '::' . __LINE__ . " User account history entry fails to be deleted." . print_r($tmv, true));
                throw new Tinebase_Exception_InvalidArgument("History log delete entry for user account fails.");
            }
        }
    }
    
    /**
     * returns all shared addressbooks
     * 
     * @return Tinebase_Record_RecordSet of shared addressbooks
     * 
     * @todo do we need to fetch ALL shared containers here (even if admin user has NO grants for them)?
     */
    public function searchSharedAddressbooks()
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        
        return Tinebase_Container::getInstance()->getSharedContainer(Tinebase_Core::getUser(), 'Addressbook', Tinebase_Model_Grants::GRANT_READ, TRUE);
    }
    
    /**
     * create or update contact in addressbook backend
     * 
     * @param  Tinebase_Model_FullUser $_user
     * @param  array $_contact
     * @return Addressbook_Model_Contact
     */
    public function createOrUpdateContact(Tinebase_Model_FullUser $_user, array $_contact = null)
    {
        $contactsBackend = Addressbook_Backend_Factory::factory(Addressbook_Backend_Factory::SQL);
        $contactsBackend->setGetDisabledContacts(true);
        
        if (empty($_user->container_id)) {
            $appConfigDefaults = Admin_Controller::getInstance()->getConfigSettings();
            $_user->container_id = $appConfigDefaults[Admin_Model_Config::DEFAULTINTERNALADDRESSBOOK];
        }
        try {
            if (empty($_user->contact_id)) { // jump to catch block
                throw new Tinebase_Exception_NotFound('contact_id is empty');
            }
            
            $contact = $contactsBackend->get($_user->contact_id);
            $contact->n_family   = $_user->accountLastName;
            $contact->n_given    = $_user->accountFirstName;
            $contact->n_fn       = $_user->accountFullName;
            $contact->n_fileas   = $_user->accountDisplayName;
            $contact->email      = $_user->accountEmailAddress;
            $contact->type       = Addressbook_Model_Contact::CONTACTTYPE_USER;
            $contact->container_id = $_user->container_id;
            if($_contact){
                $contact->tel_cell = $_contact['tel_cell'];
                $contact->tel_work = $_contact['tel_work'];
                $contact->tel_home = $_contact['tel_cell'];
                $contact->bday = $_contact['bday'];
                $contact->org_name = $_contact['org_name'];
                $contact->org_unit = $_contact['org_unit'];
                $contact->email_home = $_contact['email_home'];
                $contact->adr_one_locality =  $_contact['adr_one_locality'];
                $contact->adr_one_postalcode =  $_contact['adr_one_postalcode'];
                $contact->adr_one_street =  $_contact['adr_one_street'];
                $contact->adr_one_region =  $_contact['adr_one_region'];
            }
            
            // add modlog info
            Tinebase_Timemachine_ModificationLog::setRecordMetaData($contact, 'update');
            
            $contact = $contactsBackend->update($contact);
            
        } catch (Tinebase_Exception_NotFound $tenf) {
            // add new contact
            $contact = new Addressbook_Model_Contact(array(
                'n_family'      => $_user->accountLastName,
                'n_given'       => $_user->accountFirstName,
                'n_fn'          => $_user->accountFullName,
                'n_fileas'      => $_user->accountDisplayName,
                'email'         => $_user->accountEmailAddress,
                'type'          => Addressbook_Model_Contact::CONTACTTYPE_USER,
                'container_id'  => $_user->container_id,
           ));
            if($_contact){
                $contact->tel_cell = $_contact['tel_cell'];
                $contact->tel_work = $_contact['tel_work'];
                $contact->tel_home = $_contact['tel_cell'];
                $contact->bday = $_contact['bday'];
                $contact->org_name = $_contact['org_name'];
                $contact->org_unit = $_contact['org_unit'];
                $contact->email_home = $_contact['email_home'];
                $contact->adr_one_locality =  $_contact['adr_one_locality'];
                $contact->adr_one_postalcode =  $_contact['adr_one_postalcode'];
                $contact->adr_one_street =  $_contact['adr_one_street'];
                $contact->adr_one_region =  $_contact['adr_one_region'];
            }
            
            // add modlog info
            try{
                Tinebase_Timemachine_ModificationLog::setRecordMetaData($contact, 'create');
            }
            catch (Tinebase_Exception_UnexpectedValue $tmv){
                throw new Tinebase_Exception_NotFound('Event create fails to be added to Timemachinelog table');
            }
    
            $contact = $contactsBackend->create($contact);
        }
        
        return $contact;
    }
    
    /**
     * delete contact associated with user
     * 
     * @param string  $_contactId
     */
    protected function _deleteContact($_contactId)
    {
        $contactsBackend = Addressbook_Backend_Factory::factory(Addressbook_Backend_Factory::SQL);
        
        $contactsBackend->delete($_contactId);
    }

    /**
     *Check the availability of the email on the user backend
     *
     * @param array $_arrEmails
     * @return array
     */
    public function checkEmail(array $_arrEmails)
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        $return = $this->_userBackend->checkEmail($_arrEmails);
        return $return;
    }

    /**
    *Check the availability of the email on the user backen
    *
    * @param string $_loginName
    * @return array
    */
    public function checkExistingAccountLoginName($_loginName)
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        $status = 0;
        try {
            $returnData = $this->_userBackend->getUserByProperty('accountLoginName', $_loginName, 'Tinebase_Model_FullUser');
            $id = $returnData->accountId;
            $status = $status + 1;
        }catch (Exception $exc) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' ' . $exc->getMessage());
        }
        try {
            $returnData = $this->_userBackend->getUserByPropertyFromSyncBackend('accountLoginName', $_loginName, 'Tinebase_Model_FullUser');
            $id = $returnData->accountLoginName;
            $status = $status + 2;
        }catch (Exception $exc) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' ' . $exc->getMessage());
        }
        $return = array(
            'id'          => $id,
            'status'      => $status
        );
        return $return;
    }

    /**
     *
     * @param string $_loginName
     * @return array
     * @throws Tinebase_Exception_SystemGeneric
     */
    public function syncAccountFromUserBackend($_loginName)
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        if($this->_userBackend instanceof Tinebase_User_Ldap){
            $ldapUser = $this->_userBackend->getUserByPropertyFromSyncBackend('accountLoginName', $_loginName, 'Tinebase_Model_FullUser');
            try{
                $transactionId = Tinebase_TransactionManager::getInstance()->startTransaction(Tinebase_Core::getDb());
                $syncedUser = Tinebase_User::syncUser($ldapUser,array('syncContactData' => TRUE));
                Tinebase_TransactionManager::getInstance()->commitTransaction($transactionId);
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . 'Syncing user '.$userFromSyncBackend->accountId.' to database');
            }catch (Exception $e) {
                if(isset($transactionId)){
                    Tinebase_TransactionManager::getInstance()->rollBack();
                }
                if (Tinebase_Core::isLogLevel(Zend_Log::ERR)) Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' ' . $e->getMessage());
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' ' . $e->getTraceAsString());
                $translate = Tinebase_Translation::getTranslation('Admin');
                throw $e;
            }
            $return = array(
                'id'       => $syncedUser->accountId,
                'status'   => 'sucess'
            );
        return $return;
        }
    }

     /**
     * validate json non-empty keys
     *
     * @param string $_jsonObject        the JavaScript JSON structured data
     * @return array $result             array data with originary JSON structure wich does have data
     */
    protected function _jsonDataValid($_jsonObject)
    {
        $result = array();
        foreach ($_jsonObject as $k=>$v){
            if ((is_array($v)) || (is_object($v))){
                $recursive = $this->_jsonDataValid($v);
                if(count($recursive))
                    $result[$k]=$recursive;
            }
            else{
                if (
                     (($v !== NULL) && ($v !== ''))
                     && (strlen(trim($v))>0)
                ){
                    $result[$k] = $v;
                }
            }
        }
        return $result;
    }
}
