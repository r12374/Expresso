/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2011 Metaways Infosystems GmbH (http://www.metaways.de)
 */
 
/*global Ext, Tine*/

Ext.ns('Tine.Admin.container');

/**
 * @namespace   Tine.Admin.container
 * @class       Tine.Admin.ContainerEditDialog
 * @extends     Tine.widgets.dialog.EditDialog
 * 
 * <p>Container Edit Dialog</p>
 * <p>
 * </p>
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * 
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Admin.ContainerEditDialog
 * 
 * TODO add note for personal containers (note is sent to container owner)
 */
Tine.Admin.ContainerEditDialog = Ext.extend(Tine.widgets.dialog.EditDialog, {
    
    /**
     * @private
     */
    windowNamePrefix: 'containerEditWindow_',
    appName: 'Admin',
    recordClass: Tine.Admin.Model.Container,
    recordProxy: Tine.Admin.containerBackend,
    evalGrants: false,
    
    /**
     * executed after record got updated from proxy
     */
    onRecordLoad: function () {
        Tine.Admin.ContainerEditDialog.superclass.onRecordLoad.apply(this, arguments);
        
        // load grants store if editing record
        if (this.record && this.record.id) {
            this.grantsStore.loadData({
                results:    this.record.get('account_grants'),
                totalcount: this.record.get('account_grants').length
            });
        }
    },    
    
    /**
     * executed when record gets updated from form
     */
    onRecordUpdate: function () {
        Tine.Admin.ContainerEditDialog.superclass.onRecordUpdate.apply(this, arguments);
        
        // get grants from grants grid
        this.record.set('account_grants', '');
        var grants = [];
        this.grantsStore.each(function(grant){
            grants.push(grant.data);
        });
        this.record.set('account_grants', grants);
    },
    
    /**
     * create grants store + grid
     * 
     * @return {Tine.widgets.container.GrantsGrid}
     */
    initGrantsGrid: function () {
        this.grantsStore = new Ext.data.JsonStore({
            root: 'results',
            totalProperty: 'totalcount',
            id: 'account_id',
            fields: Tine.Tinebase.Model.Grant
        });
       
        this.grantsGrid = new Tine.widgets.container.GrantsGrid({
            flex: 1,
            store: this.grantsStore,
            grantContainer: this.record.data,
            alwaysShowAdminGrant: true,
            showHidden: true,
            columnWidth: 1,
            height: 200
        });
        
        return this.grantsGrid;
    },
    
    /**
     * returns dialog
     */
    getFormItems: function () {
        var userApplications = Tine.Tinebase.registry.get('userApplications');
        this.appStore = new Ext.data.JsonStore({
            root: 'results',
            totalProperty: 'totalcount',
            fields: Tine.Admin.Model.Application
        });
        this.appStore.loadData({
            results: userApplications,
            totalcount: userApplications.length
        });
        
        return {
            xtype: 'tabpanel',
            border: false,
            plain: true,
            activeTab: 0,
            defaults: {
                border: false,
                frame: true,
                layoutConfig: {type: 'fit', align: 'stretch', pack: 'start'}
            },
            items: [{
                title: this.app.i18n.n_('Common Configurations', 'Common Configurations', 1),
                
                items: [{
                        xtype: 'columnform',
                        border: false,
                        autoHeight: true,
                        items: [[{
                                columnWidth: 0.225,
                                fieldLabel: this.app.i18n._('Name'), 
                                name: 'name',
                                allowBlank: false,
                                maxLength: 40
                            }, {
                                xtype: 'combo',
                                readOnly: this.record.id != 0,
                                store: this.appStore,
                                columnWidth: 0.225,
                                name: 'application_id',
                                displayField: 'name',
                                valueField: 'id',
                                fieldLabel: this.app.i18n._('Application'),
                                mode: 'local',
                                anchor: '100%',
                                allowBlank: false,
                                forceSelection: true
                            }, 
                                // TODO: in master we can fill a combo using app.registry.get('models'), 
                                //        but at the moment this is just a ro textfield, and we have no 
                                //        apps with more than one model having containers
                            {
                                readOnly: 1,
                                columnWidth: 0.225,
                                name: 'model',
                                fieldLabel: this.app.i18n._('Model'),
                                anchor: '100%',
                                allowBlank: true
                            }, {
                                xtype: 'combo',
                                columnWidth: 0.225,
                                name: 'type',
                                fieldLabel: this.app.i18n._('Type'),
                                store: [['personal', this.app.i18n._('personal')], ['shared', this.app.i18n._('shared')]],
                                listeners: {
                                    scope: this,
                                    select: function (combo, record) {
                                        this.getForm().findField('note').setDisabled(record.data.field1 === 'shared');
                                    }
                                },
                                mode: 'local',
                                anchor: '100%',
                                allowBlank: false,
                                forceSelection: true
                            }, {
                                xtype: 'colorfield',
                                columnWidth: 0.1,
                                fieldLabel: this.app.i18n._('Color'),
                                name: 'color'
                            },
                            this.initGrantsGrid(), {
                                emptyText: this.app.i18n._('Note for Owner'),
                                disabled: this.record.get('type') == 'shared',
                                xtype: 'textarea',
                                border: false,
                                autoHeight: true,
                                name: 'note',
                                columnWidth: 1
                            }]]
                        }
                    ]
                },//tab common
                {
                title: this.app.i18n.n_('Backend', 'Backend', 1),
                items: [{
                    xtype: 'columnform',
                    border: false,
                    autoHeight: true,
                    items: [[
                        {
                            xtype: 'combo',
                            name: 'backend',
                            fieldLabel: this.app.i18n._('Backend'),
                            //TODO: Fetch backends from the application
                            store: [['sql', this.app.i18n._('Sql')], ['ldap', this.app.i18n._('Ldap')]],
                            mode: 'local',
                            value: 'sql',
                            allowBlank: false,
                            forceSelection: true,
                            columnWidth: 1,
                            listeners: {
                                scope: this, 
                                select: function (combo, record)
                                {                                   
                                    var Form = this.getForm();
                                    var Field = record.data.field1 === 'sql';
                                    var Fields = ['ldapHost', 'ldapPort', 'ldapSSL', 'ldapDn', 'ldapAccount'
                                        , 'ldapObjectClass', 'ldapPassword', 'ldapMaxResults', 'ldapRecursive'];
                                    //disabled as it will not be implemented now 'ldapQuickSearch'
                                    for(var index = 0; index < Fields.length; index++){
                                        Form.findField(Fields[index]).setDisabled(Field);
                                    }
                                }
                            }
                        }, {
                            xtype: 'textfield',
                            name: 'ldapHost',
                            fieldLabel: this.app.i18n._('Host'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            allowBlank: false,
                            columnWidth: 0.6
                        }, {
                            xtype: 'numberfield',
                            name: 'ldapPort',
                            fieldLabel: this.app.i18n._('Port'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            allowDecimals: false,
                            allowNegative: false,
                            maxLength: 5,
                            columnWidth: 0.2
                        }, {
                            xtype: 'combo',
                            name: 'ldapSSL',
                            fieldLabel: this.app.i18n._('Use SSL'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            store: [[0, this.app.i18n._('false')], [1, this.app.i18n._('true')]],
                            allowBlank: false,
                            forceSelection: true,
                            mode: 'local',
                            columnWidth: 0.2
                        }, {
                            xtype: 'textfield',
                            name: 'ldapDn',
                            fieldLabel: this.app.i18n._('Distinguished Name'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            allowBlank: false,
                            columnWidth: 1
                        }, {
                            xtype: 'textfield',
                            name: 'ldapObjectClass',
                            fieldLabel: this.app.i18n._('Search Filter'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            allowBlank: false,
                            columnWidth: 1
                        }, {
                            xtype: 'textfield',
                            name: 'ldapAccount',
                            fieldLabel: this.app.i18n._('Account'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            allowBlank: false,
                            columnWidth: 0.6
                        }, {
                            xtype: 'textfield',
                            inputType: 'password',
                            name: 'ldapPassword',
                            fieldLabel: this.app.i18n._('Password'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            allowBlank: false,
                            columnWidth: 0.4
                        }, {
                            xtype: 'combo',
                            name: 'ldapQuickSearch',                                
                            fieldLabel: this.app.i18n._('Quick Search'),
                            disabled: true,
                            mode: 'local',
                            store: [[0, this.app.i18n._('false')], [1, this.app.i18n._('true')]],
                            columnWidth: 0.4
                        }, {
                            xtype: 'numberfield',
                            name: 'ldapMaxResults',
                            fieldLabel: this.app.i18n._('Max Result'),
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            allowBlank: false,
                            allowDecimals: false,
                            allowNegative: false,
                            maxLength: 4,
                            columnWidth: 0.2
                        }, {
                            xtype: 'combo',
                            name: 'ldapRecursive',        
                            fieldLabel: this.app.i18n._('Recursive'), 
                            disabled: (this.record.get('backend')).toLowerCase() == 'sql',
                            store: [[0, this.app.i18n._('false')], [1, this.app.i18n._('true')]],
                            allowBlank: false,
                            forceSelection: true,
                            mode: 'local',
                            columnWidth: 0.4
                        }
                    ]]
                }]
            }]
        };
    }
});

/**
 * Container Edit Popup
 * 
 * @param   {Object} config
 * @return  {Ext.ux.Window}
 */
Tine.Admin.ContainerEditDialog.openWindow = function (config) {
    var window = Tine.WindowFactory.getWindow({
        width: 600,
        height: 400,
        name: Tine.Admin.ContainerEditDialog.prototype.windowNamePrefix + Ext.id(),
        contentPanelConstructor: 'Tine.Admin.ContainerEditDialog',
        contentPanelConstructorConfig: config
    });
    return window;
};
