<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  EmailUser
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 *
 */

/**
 * class Tinebase_EmailUser_Factory
 *
 * @package    Tinebase
 * @subpackage EmailUser
 */

class Tinebase_EmailUser_Factory
{
    private static $mailApplication = NULL;

    /**
     * Return name of configured mail application
     */
    public static function getMailApplication()
    {
        if (NULL == self::$mailApplication){
            self::$mailApplication = isset(Tinebase_Core::getConfig()->mailapplication) ? Tinebase_Core::getConfig()->mailapplication : 'Felamimail';
        }
        return self::$mailApplication;
    }

    /**
     * if class could be for instance Felamimail_Controller_Account,
     * namespaceWithoutBasePrefix is only Controller_Account
     * class must implements getInstance() method
     *
     * @param string $namespaceWithoutBasePrefix
     */
    public static function getInstance($namespaceWithoutBasePrefix)
    {
        $class = self::getMailApplication() . '_' . $namespaceWithoutBasePrefix;
        return $class::getInstance();
    }

    /**
     * if class could be for instance Felamimail_Controller_Account,
     * namespaceWithoutBasePrefix is only Controller_Account
     * class must implements getInstance() method

     * @param string $namespaceWithoutBasePrefix
     * @param string $name constant name
     */
    public static function getConstant($namespaceWithoutBasePrefix, $name)
    {
        $class = self::getMailApplication() . '_' . $namespaceWithoutBasePrefix;
        return constant($class . '::' . $name);
    }

    /**
     * Calls a static method from email application
     *
     * if class could be for instance Felamimail_Controller_Account,
     * namespaceWithoutBasePrefix is only Controller_Account
     * class must implements getInstance() method

     * @param string $namespaceWithoutBasePrefix
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public static function callStatic($namespaceWithoutBasePrefix, $method, array $args)
    {
        $class = self::getMailApplication() . '_' . $namespaceWithoutBasePrefix;
        if (!class_exists($class)) {
            $class = 'Tinebase_' . $namespaceWithoutBasePrefix;
        }
        return call_user_func_array(array($class, $method), $args);
    }

    /**
     * Adds the properly filter according e-mail application
     * @param  Tinebase_DateTime $received
     */
    public static function getPeriodFilter($received)
    {
        $mailApplication = self::getMailApplication();
        if ($mailApplication !== 'Felamimail') {
            $class =  $mailApplication . '_Model_Filter_DateTime';
            return new $class('received', 'after', $received->get(Tinebase_Record_Abstract::ISO8601LONG));
        } else {
            return new Tinebase_Model_Filter_DateTime('received', 'after', $received->get(Tinebase_Record_Abstract::ISO8601LONG));
        }
    }


}