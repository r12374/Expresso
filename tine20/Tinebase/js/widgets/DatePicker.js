/*
 * Tine 2.0
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2007-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.ns('Tine.widgets');

/**
 * Customized DatePicker component
 *
 * @namespace   Tine.widgets
 * @class       Tine.widgets.DatePicker
 * @extends     Ext.DatePicker
 */
Tine.widgets.DatePicker = Ext.extend(Ext.DatePicker, {
    /**
     * @cfg {String} todayTip
     * A string used to format the message for displaying in a tooltip over the button that
     * selects the current date. Defaults to <code>'{0}'</code> where
     * the <code>{0}</code> token is replaced by today's date.
     */
    todayTip : '{0}',
});

Ext.reg('widget-datepicker', Tine.widgets.DatePicker);