<?php

/**
 * Tine 2.0
 *
 * @package     Tinabase
 * @subpackage  AccessLog
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 *
 */

/**
 * Default strategy class for AccessLog
 *
 * @package     Tinabase
 * @subpackage  AccessLog
 */
class Tinebase_AccessLog_Strategy_Default
{

    /**
     * get client remote ip address
     *
     * @return string
     */
    public static function getRemoteIpAddress()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

}