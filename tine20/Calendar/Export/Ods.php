<?php
/**
 * Ods export generation class
 *
 * @package     Calendar
 * @subpackage  Export
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2014 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * Calendar Ods generation class
 *
 * @package     Calendar
 * @subpackage  Export
 *
 */
class Calendar_Export_Ods extends Calendar_Export_Abstract
{
    /**
     * default export definition name
     *
     * @var string
     */
    protected $_defaultExportname = 'cal_default_ods';
}
