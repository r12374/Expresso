/*****
 *	Servico Federal de Processamento de Dados - SERPRO
 *	Brazil
 *	All Rights Reserved.
 *
 *	Author: Serpro
 */

package {
	import flash.trace.Trace;
	import org.osmf.metadata.NullMetadataSynthesizer;
	import flash.utils.ByteArray;
	import flash.net.FileReference;
	import flash.display.Sprite;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.Event;

	import flash.display.Shape;

	/**
	 * Loading indicator
	 */
	public class activityPanel extends Sprite {

		private var _donutTimer:Timer;
		private var pos:int;
		private var colors:Array = 
			new Array(
						0x777777, 0x777777, 0x777777, 0x777777, 0x777777, 0x777777, 0x777777,
						0x777777, 0x777777, 0x777777, 0x777777, 0x777777, 0x777777, 0x777777,
						0x777777, 0x777777, 0x777777);

		public function activityPanel() {
			addEventListener(Event.ADDED_TO_STAGE, refreshLayout);
			addEventListener(Event.REMOVED_FROM_STAGE, removeVars);
		}

		public function removeVars(event:Event=null):void {
			_donutTimer.stop();
			_donutTimer=null;
		}

		private function refreshLayout(event:Event=null):void {
			var x:Number;
			var y:Number;
			var step:Number = 360/colors.length;
			var frame:Shape;

			frame = new Shape();
			frame.graphics.beginFill(0x0);
			frame.name = "frame";
			frame.graphics.drawRect(0,0,45,45);
			addChild(frame);

			var circleExt:Shape;
			for (var element:int=0; element<colors.length; element++) {
				circleExt = new Shape();
				circleExt.graphics.beginFill(0x0);
				circleExt.name = "shape" + element;
				circleExt.graphics.lineStyle(2, colors[element]);
				for (var i:int=element*step+15; i<(element+1)*step; i++) {
					x = Math.cos(i*Math.PI/180);
					y = Math.sin(i*Math.PI/180);
					circleExt.graphics.moveTo(45/2+20*x, 45/2-20*y);
					circleExt.graphics.lineTo(45/2+10*x, 45/2-10*y);
					trace(width + "/" + height + ": " + (width/2+20*x).toString() +"," + (width/2-20*y).toString()
						+	"até: " + (height/2+10*x).toString() +"," + (height/2-10*y).toString());
				}
				circleExt.graphics.endFill();
				addChild(circleExt);
			}

			_donutTimer = new Timer(80);
			_donutTimer.addEventListener(TimerEvent.TIMER, timerDonutCompleteHandler);
			_donutTimer.start();
			pos = 0;

		}

		private function timerDonutCompleteHandler(event:TimerEvent):void {
			var circleExt:Shape = Shape(getChildByName("shape"+pos));
			circleExt.visible = true;
			addChild(circleExt);
			pos--;
			if(pos<0)
				pos=colors.length-1;
			circleExt = Shape(getChildByName("shape"+pos));
			circleExt.visible = false;
			addChild(circleExt);
		}
	}

}
