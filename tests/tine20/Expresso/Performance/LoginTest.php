<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Expresso
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 *
 */

/**
 * Login Test
 */
require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Test class for login performance
 */
class Expresso_Performance_LoginTest extends PHPUnit_Framework_TestCase
{

    /**
     * Runs the test methods of this class.
     *
     * @access public
     * @static
     */
    public static function main()
    {
        $suite  = new PHPUnit_Framework_TestSuite('ExpressoV3 Login Test');
        PHPUnit_TextUI_TestRunner::run($suite);
    }

    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }

    /**
     * Requires into test config file:
     *
     * return array(
     *     'username' => [USERNAME]
     *     'password' => [PASSWORD]
     * );
     *
     * @throws Exception
     */
    public function testLoginPerformance()
    {
        $tinebaseController = Tinebase_Controller::getInstance();
        $time_start = microtime(true);
        if (!$tinebaseController->login($config->username, $config->password, $config->ip)){
            throw new Exception("Couldn't login, user session required for tests! \n");
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        // reference time from JMeter tests
        $this->assertLessThanOrEqual(376, $time);
    }
}
