package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.Provider;
import java.security.Security;
import java.util.List;
import java.util.logging.Logger;
import sun.security.pkcs11.SunPKCS11;

public class NSSConfig {

    private static final Logger logger = Logger.getLogger(NSSConfig.class.getName());

    private final String name;
    private final String description;
    private final String nssLibraryDirectory;
    private final String nssSecmodDirectory;
    private final DbMode nssDbMode;
    private final Module nssModule;
    private final List<Mechanism> enabledMechanisms;
    private final List<Mechanism> disabledMechanisms;
    private final Boolean showInfo;
    //TODO: attributes

    private Provider provider;

    NSSConfig(
            String name,
            String description,
            String nssLibraryDirectory,
            String nssSecmodDirectory,
            DbMode nssDbMode,
            Module nssModule,
            List<Mechanism> enabledMechanisms,
            List<Mechanism> disabledMechanisms,
            Boolean showInfo
    ) {
        this.name = name;
        this.description = description;
        this.nssLibraryDirectory = nssLibraryDirectory;
        this.nssSecmodDirectory = nssSecmodDirectory;
        this.nssDbMode = nssDbMode;
        this.nssModule = nssModule;
        this.enabledMechanisms = enabledMechanisms;
        this.disabledMechanisms = disabledMechanisms;
        this.showInfo = showInfo;

        Exception e = null;
        if( (nssLibraryDirectory != null) && (nssSecmodDirectory != null)   ) {
            try {
                provider = new SunPKCS11(new ByteArrayInputStream(toString().getBytes()));
                Security.addProvider(provider);
            }
            catch(Exception ex) {
                e = ex;
                provider = null;
            }
        }

        StringWriter sw = new StringWriter();
        sw.append("[").append(name).append(" config]\n").append(toString());
        sw.flush();
        if(e != null) {
            PrintWriter pw = new PrintWriter(sw, true);
            e.printStackTrace(pw);
            pw.flush();
        }

        System.out.println(sw.toString());
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getNssLibraryDirectory() {
        return nssLibraryDirectory;
    }

    public String getNssSecmodDirectory() {
        return nssSecmodDirectory;
    }

    public DbMode getNssDbMode() {
        return nssDbMode;
    }

    public Module getNssModule() {
        return nssModule;
    }

    public List<Mechanism> getEnabledMechanisms() {
        return enabledMechanisms;
    }

    public List<Mechanism> getDisabledMechanisms() {
        return disabledMechanisms;
    }

    public Boolean showInfo() {
        return showInfo;
    }

    public Provider getProvider() {
        return provider;
    }

    private String strValue;

    @Override
    public final String toString() {
        if(strValue != null) {
            return strValue;
        }
        StringBuilder sb = new StringBuilder();
        if(name != null) {
            sb.append("name = ").append(name).append("\n");
        }
        if(description != null) {
            sb.append("description = ").append(description).append("\n");
        }
        if(nssLibraryDirectory != null) {
            sb.append("nssLibraryDirectory = ").append(nssLibraryDirectory).append("\n");
        }
        if(nssSecmodDirectory != null) {
            sb.append("nssSecmodDirectory = ").append(nssSecmodDirectory).append("\n");
        }
        if(nssDbMode != null) {
            sb.append("nssDbMode = ").append(nssDbMode).append("\n");
        }
        if(nssModule != null) {
            sb.append("nssModule = ").append(nssModule).append("\n");
        }
        if( ! enabledMechanisms.isEmpty()) {
            sb.append("enabledMechanisms = {\n");
            for(Mechanism mechanism : enabledMechanisms) {
                sb.append("  ").append(mechanism.name()).append("\n");
            }
            sb.append("}\n");
        }
        if( ! disabledMechanisms.isEmpty()) {
            sb.append("disabledMechanisms = {\n");
            for(Mechanism mechanism : disabledMechanisms) {
                sb.append("  ").append(mechanism.name()).append("\n");
            }
            sb.append("}\n");
        }
        if(showInfo != null) {
            sb.append("showInfo = ").append(showInfo).append("\n");
        }

        return (strValue = sb.toString());
    }


    public enum DbMode {
        readWrite,
        readOnly,
        noDb;
    }

    public enum Module {
        keystore,
        crypto,
        fips,
        trustanchors;
    }

    public enum Mechanism {

        CKM_MD2,
        CKM_MD5,
        CKM_RC4,
        CKM_AES_CTR,
        CKM_RSA_X_509,
        CKM_DSA,
        CKM_DSA_SHA1,
        CKM_ECDSA,
        CKM_ECDSA_SHA1,
        CKM_SHA_1,
        CKM_SHA256,
        CKM_SHA384,
        CKM_SHA512,

        CKM_DES_CBC,
        CKM_DES3_CBC,
        CKM_AES_CBC,
        CKM_BLOWFISH_CBC,

        CKM_MD2_RSA_PKCS,
        CKM_RSA_PKCS,
        CKM_MD5_RSA_PKCS,
        CKM_SHA1_RSA_PKCS,
        CKM_SHA256_RSA_PKCS,
        CKM_SHA384_RSA_PKCS,
        CKM_SHA512_RSA_PKCS,

        CKM_RC4_KEY_GEN,
        CKM_AES_KEY_GEN,
        CKM_DES_KEY_GEN,
        CKM_DES3_KEY_GEN,
        CKM_BLOWFISH_KEY_GEN,

        CKM_RSA_PKCS_KEY_PAIR_GEN,
        CKM_DSA_KEY_PAIR_GEN,
        CKM_DH_PKCS_KEY_PAIR_GEN,
        CKM_EC_KEY_PAIR_GEN,

        CKM_ECDH1_DERIVE,
        CKM_DH_PKCS_DERIVE,

        CKM_MD5_HMAC,
        CKM_SHA_1_HMAC,
        CKM_SHA256_HMAC,
        CKM_SHA384_HMAC,
        CKM_SHA512_HMAC;

    }

}

